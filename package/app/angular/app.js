(function () {

    'use strict';

    angular.module('yeongular', [
        'yeongular.factories',
        'yeongular.services',
        'yeongular.directives',
        'yeongular.modules',
        'ui.router'
    ]);

})();