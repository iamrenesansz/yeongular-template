YeoNgular-template [![Built with Grunt](https://cdn.gruntjs.com/builtwith.png)](http://gruntjs.com/)
==================

![](http://i.imgur.com/6xQcUu4.png)

Template files for YeoNgular.

**NOTE:** You can still clone this repo if you don't know how to use [Yeoman](http://yeoman.io/).

## Installation

1. Make sure you have [NodeJS](https://nodejs.org/download/) installed.
2. Make sure you can use [Grunt](gruntjs.com/getting-started) in the command line.
3. Make sure you can use [Bower](http://bower.io/#install-bower) in the command line.

## Project Structure

* **package** - This contains the complete templates of the project.
* **factory** - This contains the template file(s) for factory.
* **module** - This contains the template file(s) for  module.
* **service** - This contains the template file(s) for service.

**and more to come....**

----------

#### Some inspiration for whom without this boiler template would not be possible:

* [ngBoilertemplate](https://github.com/ngbp/ngbp)

* [AngularJS Style Guide](https://github.com/johnpapa/angularjs-styleguide/)

* [HTML5 Boilertemplate](https://github.com/h5bp/html5-boilerplate)

**LICENSE: MIT**

**Created by: Renemari Padillo**